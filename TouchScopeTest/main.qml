import Qt 4.7
import Qt.labs.toucharea 1.0

Rectangle {
    width: 360
    height: 360
    color: "blue"

    TouchArea {
        // This TouchArea just builds a scope. Subsequent TouchAreas will be
        // mutual exclusive. (similar to the behavior of radio buttons)
        width: parent.width/3*2-20
        height: parent.height

        Rectangle {
            id: rect1
            color:  "green"
            width: parent.width/2
            height: parent.height
            anchors.left: parent.left
            anchors.top:  parent.top

            TouchArea {
                // This TouchArea will be mutual exclusive to ta2
                // due to sharing TouchArea scope.
                id: ta1
                anchors.fill: parent
                // Create bindings to touch points.
                touchPoints: [
                    TouchPoint { id: tp1 }  // tp1 will bind to the first physical
                ]                           // touch point appearing in this TouchArea.

                // Display a message whenever a new physical touch point (an
                // actual fingers on the screen) appears in this TouchArea.
                onTouchStart: {
                    console.log("TouchBegin - touches.length:", touches.length, "changedTouches.length:", changedTouches.length)
                }

                // Display a message when a touch point moved.
                // - touches[] is a list of all physical touch points that are
                //   available at the time this event occurs.
                // - changedTouches[] is a list to of touch points that have been altered
                //   with the last event. (including touch points that were released
                //   with the last event)
                onTouchMove: {
                    console.log("TouchMove - touches.length:", touches.length, "changedTouches.length:", changedTouches.length)
                }

                // Display a message whenever a physical touch point is removed.
                onTouchEnd: {
                    console.log("TouchEnd - touches.length:", touches.length, "changedTouches.length:", changedTouches.length)
                }
            }

            Rectangle {
                id: target1
                color: "black"
                width: 40
                height: 40
                pos.x: tp1.x // Bind to the position of tp1. This is what we
                pos.y: tp1.y // need to create the bindings above.
            }
        }

        Rectangle {
            id: rect2
            color:  "yellow"
            width: parent.width/2
            height: parent.height
            anchors.left: rect1.right
            anchors.top:  parent.top

            TouchArea {
                // This TouchArea will be mutual exclusive to ta1
                // due to sharing TouchArea scope.
                id: ta2
                anchors.fill: parent
                // we are not interested in binding to touch points here, therefor we omit
                // defining bindings for physical touch points.
                // touchPoints: [ TouchPoint { id: tpYellow1 }, TouchPoint { id: tpYellow2} ]
            }

            Rectangle {
                id: target2
                color: "black"
                width: 40
                height: 40
                anchors.centerIn: parent
                rotation: ta2.rotationAngle // we can still bind to properties of the TouchArea.
                scale: ta2.scaleFactor      // rotationAngle and scaleFactor are exported for convenience.
            }
        }

    }

    Rectangle {
        id: rect3
        color:  "red"
        width: parent.width/3-10
        height: parent.height
        anchors.right: parent.right
        anchors.top:  parent.top

        TouchArea {
            // This TouchArea will not be mutual exclusive to any other TouchArea,
            // because it is not sharing a TouchArea scope. (No other TouchArea in parental tree)
            id: ta3
            anchors.fill: parent

            // If we are not interested in using QML bindings, but would just like to have the
            // raw touch information - for whatever reason - we can use the list touches[].
            onTouchMove: {
                target3.pos.x = touches[0].x
                target3.pos.y = touches[0].y
            }
        }

        Rectangle {
            id: target3
            color: "black"
            width: 40
            height: 40
            pos.x: 0
            pos.y: 0
        }
    }
}
