import Qt 4.7
import Qt.labs.toucharea 1.0

Rectangle {
    anchors.fill: parent
    color: "blue"
    width: 360
    height: 360

    TouchArea {
        id:touchArea
        anchors.fill: parent

        // We are not accepting events if there are more than 2 touch points.
        minimumTouches: 1
        maximumTouches: 2

        touchPoints: [
            TouchPoint { id: tp1 },
            TouchPoint { id: tp2 }
        ]

        onTouchMove: {
//            console.log("M ..touches.length:", touches.length, "changedTouches.length:", changedTouches.length)
            rect1.x = tp1.x
            rect1.y = tp1.y

            // We need to make sure that there actually is a target for the binding tp2
            if (tp2.valid) {
//                console.log("valid!")
                rect2.x = tp2.x
                rect2.y = tp2.y
            }
        }
    }

    Rectangle {
        color:  "yellow"
        id: rect1
        pos.x: 0; pos.y: 0
        height: 40; width: 40
        Text {
            anchors.fill: parent
            text: "1"; font.pointSize: 14
        }
    }

    Rectangle {
        color: "#04ff00"
        id: rect2
        pos.x: 0; pos.y: 0
        height: 40; width: 40
        Text {
            anchors.fill: parent
            text: "2"; font.pointSize: 14
        }
    }

    Rectangle {
        x: tp1.x+10
        y: tp1.y+10
        height: 40; width: 40
        color: "#1b8000"
    }

    Rectangle {
        anchors.centerIn: parent
        height: 40; width: 40
        scale: touchArea.scaleFactor
        rotation: touchArea.rotationAngle
        color: "purple"
    }
}

