TEMPLATE = lib
CONFIG += qt plugin
QT += declarative
TARGET = $$qtLibraryTarget(qmltouchareaplugin)

TARGETPATH = Qt/labs/toucharea
QTDIR_build:DESTDIR = $$QT_BUILD_TREE/imports/$$TARGETPATH
else:DESTDIR = imports/$$TARGETPATH
target.path = $$[QT_INSTALL_IMPORTS]/$$TARGETPATH

qmldir.files += $$PWD/qmldir
qmldir.path +=  $$[QT_INSTALL_IMPORTS]/$$TARGETPATH

HEADERS += \
    touchareaplugin.h \
    qdeclarativetoucharea.h

SOURCES += \
    touchareaplugin.cpp \
    qdeclarativetoucharea.cpp

QMAKE_POST_LINK=$(COPY_FILE) $$PWD/qmldir $$DESTDIR

INSTALLS += target qmldir

symbian {
    TARGET.EPOCALLOWDLLDATA = 1
}

